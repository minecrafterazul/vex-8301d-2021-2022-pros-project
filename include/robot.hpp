#pragma once

#include "imuController/chassisControllerPidImu.hpp"
#include "okapi/api/device/motor/abstractMotor.hpp"
#include "okapi/impl/chassis/controller/chassisControllerBuilder.hpp"
#include "okapi/impl/device/controller.hpp"
#include "okapi/impl/device/motor/motorGroup.hpp"
#include "okapi/impl/util/configurableTimeUtilFactory.hpp"

extern okapi::MotorGroup left;
extern okapi::MotorGroup right;
extern okapi::Motor armLift;
extern okapi::Motor armClamp;
extern okapi::Motor tilt;
extern okapi::Motor intake;

extern okapi::IMU imu;

extern std::shared_ptr<okapi::ChassisControllerPIDIMU> base;

extern std::shared_ptr<okapi::AsyncPositionController<double, double>>
    armLiftCtrl;
extern std::shared_ptr<okapi::AsyncPositionController<double, double>>
    armClampCtrl;
extern std::shared_ptr<okapi::AsyncPositionController<double, double>> tiltCtrl;
extern std::shared_ptr<okapi::AsyncPositionController<double, double>>
    intakeCtrl;

extern okapi::Controller master;
