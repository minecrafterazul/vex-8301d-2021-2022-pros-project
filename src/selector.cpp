#include "selector.hpp"
#include "main.h"

using namespace okapi;

static bool autonSelected;

static void updateScreen() {
	pros::lcd::set_text(0, "Auton: ");
	pros::lcd::set_text(2, getAutonDescription());
	master.clear();
	pros::delay(50);
	master.setText(0, 0, getAutonDescription());
}

void onLeftButton() {
	selectAuton(false);
	updateScreen();
}

void onCenterButton() {
	pros::lcd::shutdown();
	master.clear();
	autonSelected = true;
}

void onRightButton() {
	selectAuton(true);
	updateScreen();
}

void selectorInitialize() {
	autonSelected = false;
	ControllerButton leftBtn = master[ControllerDigital::left];
	ControllerButton rightBtn = master[ControllerDigital::right];
	ControllerButton selectBtn = master[ControllerDigital::A];
	ControllerButton cancelBtn = master[ControllerDigital::B];

	pros::lcd::initialize();
	updateScreen();
	pros::lcd::register_btn0_cb(onLeftButton);
	pros::lcd::register_btn1_cb(onCenterButton);
	pros::lcd::register_btn2_cb(onRightButton);

	while (!autonSelected) {
		if (cancelBtn.changedToPressed() || pros::competition::is_connected()) {
			onCenterButton();
		}
		if (selectBtn.changedToPressed()) {
			autonomous();
			onCenterButton();
		}
		if (leftBtn.changedToPressed()) {
			onLeftButton();
		}
		if (rightBtn.changedToPressed()) {
			onRightButton();
		}
		pros::delay(50);
	}
}
