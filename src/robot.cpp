#include "robot.hpp"
#include "okapi/impl/control/async/asyncPosControllerBuilder.hpp"

using namespace okapi;

MotorGroup left{{13, true, AbstractMotor::gearset::blue,
                 AbstractMotor::encoderUnits::counts},
                {10, true, AbstractMotor::gearset::blue,
                 AbstractMotor::encoderUnits::counts}};
MotorGroup right{{12, false, AbstractMotor::gearset::blue,
                  AbstractMotor::encoderUnits::counts},
                 {9, false, AbstractMotor::gearset::blue,
                  AbstractMotor::encoderUnits::counts}};
Motor armLift(-15);
Motor armClamp(1);

Motor tilt(11);
Motor intake(14, true, AbstractMotor::gearset::blue,
             AbstractMotor::encoderUnits::counts);

IMU imu(16);

std::shared_ptr<okapi::ChassisControllerPIDIMU> base =
    std::make_shared<ChassisControllerPIDIMU>(
        TimeUtilFactory().create(),
        std::make_shared<SkidSteerModel>(
            std::make_shared<MotorGroup>(left),
            std::make_shared<MotorGroup>(right), left.getEncoder(),
            right.getEncoder(), toUnderlyingType(AbstractMotor::gearset::blue),
            12000),
        std::make_shared<IMU>(imu),
        std::make_unique<IterativePosPIDController>(
            IterativePosPIDController::Gains{0.001, 0, 0},
            ConfigurableTimeUtilFactory(10, 2).create()),
        std::make_unique<IterativePosPIDController>(
            IterativePosPIDController::Gains{0.01, 0, 0},
            ConfigurableTimeUtilFactory(0.5, 0.1).create()),
        std::make_unique<IterativePosPIDController>(
            IterativePosPIDController::Gains{0.01, 0, 0},
            ConfigurableTimeUtilFactory(0.5, 0.1).create()),
        AbstractMotor::gearset::blue,
        ChassisScales({4_in, 14.5_in}, imev5GreenTPR));

std::shared_ptr<AsyncPositionController<double, double>> armLiftCtrl =
    AsyncPosControllerBuilder().withMotor(armLift).build();
std::shared_ptr<AsyncPositionController<double, double>> armClampCtrl =
    AsyncPosControllerBuilder().withMotor(armClamp).build();
std::shared_ptr<AsyncPositionController<double, double>> tiltCtrl =
    AsyncPosControllerBuilder().withMotor(tilt).build();
std::shared_ptr<AsyncPositionController<double, double>> intakeCtrl =
    AsyncPosControllerBuilder().withMotor(intake).build();

Controller master(ControllerId::master);
