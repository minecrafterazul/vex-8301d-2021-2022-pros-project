#include "autonomous.hpp"
#include "main.h"

#include <list>

using namespace okapi;

void neutralRight() {
	armLift.moveVelocity(-0.1 * toUnderlyingType(armLift.getGearing()));
	armClampCtrl->setTarget(1000);
	base->getModel()->driveVectorVoltage(-12000, 0);
	pros::delay(1050);
	base->getModel()->driveVectorVoltage(0, 0);
	armClampCtrl->setTarget(0);
	pros::delay(350);
	base->moveDistance(2.25_ft);
	armLift.moveVelocity(0);
	armLiftCtrl->setTarget(100);
	armLiftCtrl->waitUntilSettled();
	base->turnAngle(135_deg);
	armClampCtrl->setTarget(1000);
	pros::delay(250);
	base->turnAngle(-40_deg);
	armLift.moveVelocity(-0.1 * toUnderlyingType(armLift.getGearing()));
	base->moveDistance(-3.1_ft);
	armClampCtrl->setTarget(0);
	pros::delay(250);
	base->moveDistance(3.4_ft);
}

void allianceRight() {
	armLift.moveVelocity(-0.1 * toUnderlyingType(armLift.getGearing()));
	armClampCtrl->setTarget(1000);
	base->getModel()->driveVectorVoltage(-12000, 0);
	pros::delay(1050);
	base->getModel()->driveVectorVoltage(0, 0);
	armClampCtrl->setTarget(0);
	pros::delay(350);
	base->moveDistanceAsync(2.1_ft);
	pros::delay(500);
	armLift.moveVelocity(0);
	armLiftCtrl->setTarget(100);
	base->waitUntilSettled();
	base->turnAngleAsync(-90_deg);
	tiltCtrl->setTarget(-2700);
	base->waitUntilSettled();
	base->moveDistanceAsync(1_ft);
	pros::delay(1000);
	tiltCtrl->setTarget(-1350);
	base->moveDistanceAsync(-1.6_ft);
	tiltCtrl->waitUntilSettled();
	intake.moveVelocity(-0.5 * toUnderlyingType(intake.getGearing()));
	pros::delay(2000);
	intake.moveVelocity(0);
	tiltCtrl->setTarget(-2700);
	tiltCtrl->waitUntilSettled();
	base->moveDistanceAsync(-0.5_ft);
}

void allianceLeft() {
	double originalMax = base->getMaxVelocity();

	armLift.moveVelocity(-0.1 * toUnderlyingType(armLift.getGearing()));
	armClampCtrl->setTarget(1000);
	base->getModel()->driveVectorVoltage(-12000, 0);
	pros::delay(1150);
	base->getModel()->driveVectorVoltage(0, 0);
	armClampCtrl->setTarget(0);
	pros::delay(350);
	armLift.moveVelocity(0);
	base->moveDistanceAsync(4_ft);
	pros::delay(1900);
	base->stop();
	base->getModel()->tank(0, 12000);
	pros::delay(400);
	base->stop();
	armLiftCtrl->setTarget(100);
	base->setMaxVelocity(originalMax);
	base->setImuInitialReading(imu.controllerGet());
	base->moveDistance(-0.25_ft);
	base->turnAngle(-90_deg);
	tiltCtrl->setTarget(-2700);
	tiltCtrl->waitUntilSettled();
	base->moveDistanceAsync(0.8_ft);
	pros::delay(1000);
	base->moveDistanceAsync(-0.6_ft);
	tiltCtrl->setTarget(-1300);
	tiltCtrl->waitUntilSettled();
	intake.moveVelocity(-0.5 * toUnderlyingType(intake.getGearing()));
	pros::delay(1500);
	intake.moveVelocity(0);
	tiltCtrl->setTarget(-2700);
	tiltCtrl->waitUntilSettled();
	base->moveDistanceAsync(-0.1_ft);
}

void skills() {
	double originalMax = base->getMaxVelocity();

	tiltCtrl->setTarget(-2700);
	base->moveDistance(1.6_ft);
	base->turnAngle(90_deg);
	base->setMaxVelocity(0.75 * originalMax);
	base->moveDistanceAsync(2_ft);
	pros::delay(1000);
	base->stop();
	base->setMaxVelocity(originalMax);
	armClampCtrl->setTarget(1000);
	tiltCtrl->setTarget(-1300);
	base->moveDistance(-1.3_ft);
	base->turnAngle(180_deg);
	intake.moveVelocity(-0.5 * toUnderlyingType(intake.getGearing()));
	base->moveDistance(-1.5_ft);
	armClampCtrl->setTarget(0);
	intake.moveVelocity(0);
	pros::delay(1000);

	base->setMaxVelocity(0.75 * originalMax);
	armLiftCtrl->setTarget(3500);
	base->turnAngle(147.5_deg);
	base->moveDistance(-3.7_ft);
	armLiftCtrl->setTarget(2500);
	pros::delay(1000);
	armClampCtrl->setTarget(1000);
	pros::delay(250);
	armLiftCtrl->setTarget(4000);
	pros::delay(1000);
	base->moveDistance(0.4_ft);

	base->setMaxVelocity(originalMax);
	base->turnAngle(90_deg);
	base->moveDistance(-0.25_ft);
	armLiftCtrl->setTarget(0);
	tiltCtrl->setTarget(-2700);
	tiltCtrl->waitUntilSettled();
	base->moveDistance(-2.75_ft);
	base->turnAngle(-90_deg);

	base->setMaxVelocity(0.75 * originalMax);
	base->moveDistanceAsync(2_ft);
	pros::delay(1000);
	base->stop();
	armClampCtrl->setTarget(1000);
	tiltCtrl->setTarget(-1300);
	base->turnAngle(-90_deg);
	base->moveDistance(-4.5_ft);
	armClampCtrl->setTarget(0);
	pros::delay(500);
	armLiftCtrl->setTarget(2500);
	armLiftCtrl->waitUntilSettled();
	base->turnAngle(170_deg);
	armClampCtrl->setTarget(1000);
	pros::delay(250);
	armLiftCtrl->setTarget(4000);
	tiltCtrl->setTarget(-2700);
	pros::delay(1000);

	base->setMaxVelocity(originalMax);
	base->turnAngle(-145_deg);
	armLiftCtrl->setTarget(0);
	base->moveDistance(4_ft);
	base->getModel()->driveVectorVoltage(0, -12000);
	pros::delay(1000);
	base->getModel()->stop();
	base->moveDistance(-6_ft);
}

void nothing() {
}

typedef struct {
	std::string description;
	void (*action)(void);
} Auton;

std::list<Auton> autons = {{"Neutral right ", neutralRight},
                           {"Alliance right ", allianceRight},
                           {"Alliance left  ", allianceLeft},
                           {"Skills         ", skills},
                           {"Nothing        ", nothing}};
std::list<Auton>::iterator autonIterator = autons.begin();

void selectAuton(bool next) {
	if (next && std::next(autonIterator) != autons.end()) {
		std::advance(autonIterator, 1);
	} else if (!next && autonIterator != autons.begin()) {
		std::advance(autonIterator, -1);
	}
}

std::string getAutonDescription() {
	return autonIterator->description;
}

void autonomous() {
	autonIterator->action();
}
