/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
#include "imuController/chassisControllerPidImu.hpp"
#include "okapi/api/util/mathUtil.hpp"
#include <cmath>
#include <utility>

namespace okapi {
ChassisControllerPIDIMU::ChassisControllerPIDIMU(
    TimeUtil itimeUtil, std::shared_ptr<ChassisModel> ichassisModel,
    std::shared_ptr<IMU> iimu,
    std::unique_ptr<IterativePosPIDController> idistanceController,
    std::unique_ptr<IterativePosPIDController> iturnController,
    std::unique_ptr<IterativePosPIDController> iangleController,
    const AbstractMotor::GearsetRatioPair& igearset,
    const ChassisScales& iscales, std::shared_ptr<Logger> ilogger)
    : logger(std::move(ilogger)), chassisModel(std::move(ichassisModel)),
      imu(std::move(iimu)), timeUtil(std::move(itimeUtil)),
      distancePid(std::move(idistanceController)),
      turnPid(std::move(iturnController)),
      anglePid(std::move(iangleController)), scales(iscales),
      gearsetRatioPair(igearset) {
	if (igearset.ratio == 0) {
		std::string msg("ChassisControllerPIDIMU: The gear ratio cannot be zero! "
		                "Check if you are using "
		                "integer division.");
		LOG_ERROR(msg);
		throw std::invalid_argument(msg);
	}
	chassisModel->setGearing(igearset.internalGearset);
	chassisModel->setEncoderUnits(AbstractMotor::encoderUnits::counts);
}

ChassisControllerPIDIMU::~ChassisControllerPIDIMU() {
	dtorCalled.store(true, std::memory_order_release);
	delete task;
}

void ChassisControllerPIDIMU::loop() {
	LOG_INFO_S("Started ChassisControllerPIDIMU task.");

	auto encStartVals = chassisModel->getSensorVals();
	std::valarray<std::int32_t> encVals;
	double imuStartVal = imu->controllerGet();
	double imuVal;
	double imuTarget;
	double distanceElapsed = 0, angleChange = 0;
	modeType pastMode = none;
	auto rate = timeUtil.getRate();

	while (!dtorCalled.load(std::memory_order_acquire) && !task->notifyTake(0)) {
		/**
		 * doneLooping is set to false by moveDistanceAsync and turnAngleAsync and
		 * then set to true by waitUntilSettled
		 */
		if (doneLooping.load(std::memory_order_acquire)) {
			doneLoopingSeen.store(true, std::memory_order_release);
		} else {
			if (mode != pastMode || newMovement.load(std::memory_order_acquire)) {
				encStartVals = chassisModel->getSensorVals();
				newMovement.store(false, std::memory_order_release);
			}

			switch (mode) {
			case distance:
				encVals = chassisModel->getSensorVals() - encStartVals;
				distanceElapsed = static_cast<double>((encVals[0] + encVals[1])) / 2.0;

				imuVal = imu->controllerGet();
				imuTarget = anglePid->getTarget();

				if ((imuTarget - imuVal) > 180) {
					imuVal += 360;
				} else if ((imuTarget - imuVal) < -180) {
					imuVal -= 360;
				}

				distancePid->step(distanceElapsed);
				anglePid->step(imuVal);

				if (velocityMode) {
					chassisModel->driveVector(distancePid->getOutput(),
					                          anglePid->getOutput());
				} else {
					chassisModel->driveVectorVoltage(distancePid->getOutput(),
					                                 anglePid->getOutput());
				}

				break;

			case angle:
				imuVal = imu->controllerGet();
				imuTarget = turnPid->getTarget();

				if ((imuTarget - imuVal) > 180) {
					imuVal += 360;
				} else if ((imuTarget - imuVal) < -180) {
					imuVal -= 360;
				}

				turnPid->step(imuVal);

				if (velocityMode) {
					chassisModel->driveVector(0, turnPid->getOutput());
				} else {
					chassisModel->driveVectorVoltage(0, turnPid->getOutput());
				}

				break;

			default:
				break;
			}

			pastMode = mode;
		}

		rate->delayUntil(threadSleepTime);
	}

	stop();

	LOG_INFO_S("Stopped ChassisControllerPIDIMU task.");
}

void ChassisControllerPIDIMU::trampoline(void* context) {
	if (context) {
		static_cast<ChassisControllerPIDIMU*>(context)->loop();
	}
}

void ChassisControllerPIDIMU::moveDistanceAsync(const QLength itarget) {
	LOG_INFO("ChassisControllerPIDIMU: moving " +
	         std::to_string(itarget.convert(meter)) + " meters");
	LOG_DEBUG("ChassisControllerPIDIMU: straight " +
	          std::to_string(scales.straight) + " ratio " +
	          std::to_string(gearsetRatioPair.ratio));

	distancePid->reset();
	anglePid->reset();
	distancePid->flipDisable(false);
	anglePid->flipDisable(false);
	turnPid->flipDisable(true);
	mode = distance;

	const double newTarget =
	    itarget.convert(meter) * scales.straight * gearsetRatioPair.ratio;

	LOG_INFO("ChassisControllerPIDIMU: moving " + std::to_string(newTarget) +
	         " motor ticks");

	distancePid->setTarget(newTarget);
	anglePid->setTarget(imu->controllerGet());

	doneLooping.store(false, std::memory_order_release);
	newMovement.store(true, std::memory_order_release);
}

void ChassisControllerPIDIMU::moveRawAsync(const double itarget) {
	// Divide by straightScale so the final result turns back into motor ticks
	moveDistanceAsync((itarget / scales.straight) * meter);
}

void ChassisControllerPIDIMU::moveDistance(const QLength itarget) {
	moveDistanceAsync(itarget);
	waitUntilSettled();
}

void ChassisControllerPIDIMU::moveRaw(const double itarget) {
	// Divide by straightScale so the final result turns back into motor ticks
	moveDistance((itarget / scales.straight) * meter);
}

double periodicallyEfficient(double angle) {
	while (std::abs(angle) > 180) {
		if (angle > 180) {
			angle -= 360;
		} else if (angle < -180) {
			angle += 360;
		}
	}
	return angle;
}

void ChassisControllerPIDIMU::turnAngleAsync(const QAngle idegTarget) {
	LOG_INFO("ChassisControllerPIDIMU: turning " +
	         std::to_string(idegTarget.convert(degree)) + " degrees");
	LOG_DEBUG("ChassisControllerPIDIMU: scales.turn " +
	          std::to_string(scales.turn) + " ratio " +
	          std::to_string(gearsetRatioPair.ratio));

	turnPid->reset();
	turnPid->flipDisable(false);
	distancePid->flipDisable(true);
	anglePid->flipDisable(true);
	mode = angle;

	const double newTarget = idegTarget.convert(degree) * boolToSign(normalTurns);

	LOG_INFO("ChassisControllerPIDIMU: turning " + std::to_string(newTarget) +
	         " degrees");

	turnPid->setTarget(periodicallyEfficient(newTarget) + imuInitialReading);

	doneLooping.store(false, std::memory_order_release);
	newMovement.store(true, std::memory_order_release);
}

void ChassisControllerPIDIMU::turnRawAsync(const double idegTarget) {
	// Divide by turnScale so the final result turns back into motor ticks
	turnAngleAsync((idegTarget / scales.turn) * degree);
}

void ChassisControllerPIDIMU::turnAngle(const QAngle idegTarget) {
	turnAngleAsync(idegTarget);
	waitUntilSettled();
}

void ChassisControllerPIDIMU::turnRaw(const double idegTarget) {
	// Divide by turnScale so the final result turns back into motor ticks
	turnAngle((idegTarget / scales.turn) * degree);
}

void ChassisControllerPIDIMU::setImuInitialReading(double iimuInitialReading) {
	imuInitialReading = iimuInitialReading;
}

void ChassisControllerPIDIMU::setTurnsMirrored(const bool ishouldMirror) {
	normalTurns = !ishouldMirror;
}

bool ChassisControllerPIDIMU::isSettled() {
	switch (mode) {
	case distance:
		return distancePid->isSettled() && anglePid->isSettled();

	case angle:
		return turnPid->isSettled();

	default:
		return true;
	}
}

void ChassisControllerPIDIMU::waitUntilSettled() {
	LOG_INFO_S("ChassisControllerPIDIMU: Waiting to settle");

	bool completelySettled = false;

	while (!completelySettled) {
		switch (mode) {
		case distance:
			completelySettled = waitForDistanceSettled();
			break;

		case angle:
			completelySettled = waitForAngleSettled();
			break;

		default:
			completelySettled = true;
			break;
		}
	}

	// Order here is important
	mode = none;
	doneLooping.store(true, std::memory_order_release);
	doneLoopingSeen.store(false, std::memory_order_release);

	// Wait for the thread to finish if it happens to be writing to motors
	auto rate = timeUtil.getRate();
	while (!doneLoopingSeen.load(std::memory_order_acquire)) {
		rate->delayUntil(threadSleepTime);
	}

	// Stop after the thread has run at least once
	stopAfterSettled();

	LOG_INFO_S("ChassisControllerPIDIMU: Done waiting to settle");
}

bool ChassisControllerPIDIMU::waitForDistanceSettled() {
	LOG_INFO_S("ChassisControllerPIDIMU: Waiting to settle in distance mode");

	auto rate = timeUtil.getRate();
	while (!(distancePid->isSettled() && anglePid->isSettled())) {
		if (mode == angle) {
			// False will cause the loop to re-enter the switch
			LOG_WARN_S(
			    "ChassisControllerPIDIMU: Mode changed to angle while waiting in "
			    "distance!");
			return false;
		}

		rate->delayUntil(10_ms);
	}

	// True will cause the loop to exit
	return true;
}

bool ChassisControllerPIDIMU::waitForAngleSettled() {
	LOG_INFO_S("ChassisControllerPIDIMU: Waiting to settle in angle mode");

	auto rate = timeUtil.getRate();
	while (!turnPid->isSettled()) {
		if (mode == distance) {
			// False will cause the loop to re-enter the switch
			LOG_WARN_S(
			    "ChassisControllerPIDIMU: Mode changed to distance while waiting "
			    "in angle!");
			return false;
		}

		rate->delayUntil(10_ms);
	}

	// True will cause the loop to exit
	return true;
}

void ChassisControllerPIDIMU::stopAfterSettled() {
	distancePid->flipDisable(true);
	anglePid->flipDisable(true);
	turnPid->flipDisable(true);
	chassisModel->stop();
}

ChassisScales ChassisControllerPIDIMU::getChassisScales() const {
	return scales;
}

AbstractMotor::GearsetRatioPair
ChassisControllerPIDIMU::getGearsetRatioPair() const {
	return gearsetRatioPair;
}

void ChassisControllerPIDIMU::setVelocityMode(bool ivelocityMode) {
	velocityMode = ivelocityMode;
}

void ChassisControllerPIDIMU::setGains(
    const okapi::IterativePosPIDController::Gains& idistanceGains,
    const okapi::IterativePosPIDController::Gains& iturnGains,
    const okapi::IterativePosPIDController::Gains& iangleGains) {
	distancePid->setGains(idistanceGains);
	turnPid->setGains(iturnGains);
	anglePid->setGains(iangleGains);
}

std::tuple<IterativePosPIDController::Gains, IterativePosPIDController::Gains,
           IterativePosPIDController::Gains>
ChassisControllerPIDIMU::getGains() const {
	return std::make_tuple(distancePid->getGains(), turnPid->getGains(),
	                       anglePid->getGains());
}

void ChassisControllerPIDIMU::startThread() {
	if (!task) {
		task = new CrossplatformThread(trampoline, this, "ChassisControllerPIDIMU");
	}
}

CrossplatformThread* ChassisControllerPIDIMU::getThread() const {
	return task;
}

void ChassisControllerPIDIMU::stop() {
	LOG_INFO_S("ChassisControllerPIDIMU: Stopping");

	mode = none;
	doneLooping.store(true, std::memory_order_release);
	stopAfterSettled();
}

void ChassisControllerPIDIMU::setMaxVelocity(double imaxVelocity) {
	chassisModel->setMaxVelocity(imaxVelocity);
}

double ChassisControllerPIDIMU::getMaxVelocity() const {
	return chassisModel->getMaxVelocity();
}

std::shared_ptr<ChassisModel> ChassisControllerPIDIMU::getModel() {
	return chassisModel;
}

ChassisModel& ChassisControllerPIDIMU::model() {
	return *chassisModel;
}
} // namespace okapi
