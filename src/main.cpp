#include "main.h"

/**
 * Runs initialization code. This occurs as soon as the program is started.
 *
 * All other competition modes are blocked by initialize; it is recommended
 * to keep execution time for this mode under a few seconds.
 */
void initialize() {
	base->startThread();

	armLift.setBrakeMode(AbstractMotor::brakeMode::hold);
	armClamp.setBrakeMode(AbstractMotor::brakeMode::hold);
	tilt.setBrakeMode(AbstractMotor::brakeMode::hold);
	intake.setBrakeMode(AbstractMotor::brakeMode::hold);

	selectorInitialize();
}

static bool isForwardDrive = false;
static bool clampToggle = false;

static void updateScreen() {
	master.setText(
	    0, 0, "Mode: " + std::string(isForwardDrive ? "Forward" : "Reversed"));
}

/**
 * Runs the operator control code. This function will be started in its own task
 * with the default priority and stack size whenever the robot is enabled via
 * the Field Management System or the VEX Competition Switch in the operator
 * control mode.
 *
 * If no competition control is connected, this function will run immediately
 * following initialize().
 *
 * If the robot is disabled or communications is lost, the
 * operator control task will be stopped. Re-enabling the robot will restart the
 * task, not resume it from where it left off.
 */
void opcontrol() {
	base->setMaxVelocity(toUnderlyingType(AbstractMotor::gearset::blue));
	std::shared_ptr<ChassisModel> model = base->getModel();
	updateScreen();
	master[ControllerDigital::B].changedToPressed(); // Check if button is pressed
	master[ControllerDigital::Y].changedToPressed(); // Check if button is pressed
	armClamp.setBrakeMode(AbstractMotor::brakeMode::coast);

	while (true) {
		if (master[ControllerDigital::B].changedToPressed()) {
			isForwardDrive = !isForwardDrive;
			updateScreen();
		}
		if (master[ControllerDigital::Y].changedToPressed()) {
			clampToggle = !clampToggle;
		}

		model->arcade(0.8 * boolToSign(isForwardDrive) *
		                  master.getAnalog(ControllerAnalog::rightY),
		              0.8 * master.getAnalog(ControllerAnalog::rightX));
		intake.moveVelocity(0.45 * toUnderlyingType(intake.getGearing()) *
		                    master.getAnalog(ControllerAnalog::leftY));
		armLift.moveVelocity(toUnderlyingType(armLift.getGearing()) *
		                     (master.getDigital(ControllerDigital::L1) -
		                      master.getDigital(ControllerDigital::L2)));
		std::int16_t tiltPwr = toUnderlyingType(tilt.getGearing()) *
		                       (master.getDigital(ControllerDigital::R1) -
		                        master.getDigital(ControllerDigital::R2));
		if (((tiltPwr > 0) && (tilt.getPosition() < -1100)) ||
		    ((tiltPwr < 0) && (tilt.getPosition() > -2700))) {
			tilt.moveVelocity(tiltPwr);
		} else {
			tilt.moveVelocity(0);
		}
		armClamp.moveAbsolute(clampToggle ? 1000 : 300,
		                      toUnderlyingType(armClamp.getGearing()));
	}
}
